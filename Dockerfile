FROM tiangolo/uwsgi-nginx:python3.8

COPY . /app/
COPY run.py /app/main.py
COPY requirements.txt /requirements.txt
COPY prestart.sh /app/

RUN pip install -r /requirements.txt


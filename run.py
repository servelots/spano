#!/usr/bin/env python

from spano.app import create_app

def main():
  app = create_app()
  app.run(host='0.0.0.0', debug=True, port=5000)

application = create_app()

if __name__ == '__main__':
    main()
